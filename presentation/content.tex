\frame{\titlepage}

\begin{frame}
    \frametitle{Overview}
    \tableofcontents
\end{frame}

\begin{frame}
    \vfill
    \centering
    What about Julia and Python?
    \vfill
\end{frame}

\section{Introduction to LLVM}

\begin{frame}
    \frametitle{Literature for this part}
    \begin{itemize}
        \item \scriptsize\bibentry{Lattner:2004}
        \item \scriptsize\bibentry{Lattner:2012}
        \item \scriptsize\bibentry{Lattner:2008:slides}
        \item \scriptsize\bibentry{LLVM:2015}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Background: Classical Compiler Design}
    \begin{itemize}
        \item Frontend
            \begin{itemize}
                \item Parses source code files
                \item Creates intermediate representation (e.g. AST)
            \end{itemize}
        \item Optimizer
            \begin{itemize}
                \item Performs analysis and transformations on IR
            \end{itemize}
        \item Backend
            \begin{itemize}
                \item Emits machine code for target architecture
            \end{itemize}
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{resources/SimpleCompiler.png}
        \caption{Figure adopted from Lattner \cite{Lattner:2012}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{LLVM}
    \begin{itemize}
        \item Low Level Virtual Machine
        \item Language-independent compiler framework
        \item Set of libraries that implement parts of a compiler
        \item Whole framework centered around the \emph{LLVM Intermediate
            Representation} (LLVM IR)
        \item Possible use cases:
            \begin{itemize}
                \item Use all/individual LLVM components to write compilers
                \item Write additional LLVM components to be used by others
                \item JIT-compilation and dynamic reoptimization
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LLVM Intermediate Representation}
    \begin{itemize}
        \item Low-level instruction set for a virtual RISC machine
        \item Values reside in (infinitely many) virtual registers or in memory
        \item Load/Store Architecture, i.e. instructions operate on registers
        \item Virtual registers are in Static Single Assignment (SSA) form
            \begin{itemize}
                \item Each register is written only once
                \item Each register use is dominated by its definition
            \end{itemize}
        \item SSA form simplifies data-flow analyses and transformations
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LLVM Intermediate Representation (cont'd)}
    Three (isomorphic) manifestations:
    \begin{itemize}
        \item In-memory data-structure used internally for analysis and
            transformations
        \item Plain-text assembly format (stored in \verb+.ll+ files)
        \item Binary representation, also called \emph{bitcode} (stored in \verb+.bc+ files)
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Example: LLVM IR Assembly}
\begin{lstlisting}[language=llvm]
; Simple Integer Addition
define i32 @sum(i32 %a, i32 %b) {
  %tmp = add i32 %a, %b
  ret i32 %tmp
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LLVM Type System}
    \begin{itemize}
        \item Strong type system
        \item Two major classes of types:
            \begin{itemize}
                \item Primitive types
                \item Derived types
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LLVM Type System: Primitive Types}
    \begin{itemize}
        \item Arbitrary integer types \lstinline!iN!, $\text{\lstinline!N!} \in
            \lbrace 1, 2, 3, \dots, 2^{32}-1 \rbrace$
\begin{lstlisting}[language=llvm]
i1          ; single-bit integer
i8          ; 8-bit integer
i1942652    ; 1942652-bit integer
\end{lstlisting}
        \pause
        \item Floating point types
\begin{lstlisting}[language=llvm]
half        ; 16-bit floating point
float       ; 32-bit floating point
double      ; 64-bit floating point
fp128       ; 128-bit floating point
\end{lstlisting}
        \item \dots
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{LLVM Type System: Derived Types}
    \begin{itemize}
        \item Vectors
\begin{lstlisting}[language=llvm]
<5 x i32>       ; 5x32 bit integer vector
<4 x double>    ; 4x64 bit floating point vector
\end{lstlisting}
        \pause
        \item Arrays
\begin{lstlisting}[language=llvm]
[5 x i32]       ; 5x32-bit integer array
[4 x double]    ; 4x64-bit floating point array
[3 x [4 x i32]] ; nested array
\end{lstlisting}
        \pause
        \item Structures
\begin{lstlisting}[language=llvm]
{ i64, float }  ; structure type
\end{lstlisting}
        \pause
        \item Pointers
\begin{lstlisting}[language=llvm]
i32 *           ; pointer to 32-bit integer
[5 x i32] *     ; pointer to array
\end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Projects that use LLVM}
    \begin{itemize}
        \item Clang\\
            C/C++/\dots\ frontend
        \item DragonEgg\\
            LLVM as optimizer and backend for GCC (replaces LLVM-GCC)
        \item LLDB\\
            C/C++/\dots\ debugger (default on Mac OS X)
        \item Polly\\
            Polyhedral optimizations in LLVM
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{SPECINT 2000 Compile Time}
In seconds (lower is better)
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
    scale only axis,
    ybar,
    ymax=200,
    ymin=0,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    symbolic x coords={-O0,-O1,-O2,-O3,-O4},
    xtick=data,
    width  = 0.93\textwidth,
    height = 5cm,
    ybar=2*\pgflinewidth,
    ]

    \addplot[style={TuWienBlue,fill=TuWienBlue,mark=none}]
    coordinates {(-O0,79) (-O1,133) (-O2,164) (-O3,187) (-O4,0)};

    \addplot[style={ggreen,fill=ggreen,mark=none}]
    coordinates {(-O0,74) (-O1,112) (-O2,126) (-O3,131) (-O4,144)};

    \legend{GCC 4.2, LLVM-GCC 4.2}
\end{axis}
\end{tikzpicture}
\caption{Figure adopted from Lattner \cite{Lattner:2008:slides}}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{SPECINT 2000 Execution Time}
Relative to GCC -O2 (lower is faster)
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
    scale only axis,
    ybar,
    ymax=100,
    ymin=0,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    symbolic x coords={-O0,-O1,-O2,-O3,-O4},
    xtick=data,
    width  = 0.93\textwidth,
    height = 5cm,
    ybar=2*\pgflinewidth,
    yticklabel=\pgfmathparse{\tick}\pgfmathprintnumber{\pgfmathresult}\,\%,
    ]

    \addplot[style={TuWienBlue,fill=TuWienBlue,mark=none}]
    coordinates {(-O2,100) (-O3,96.3) (-O4,0)};

    \addplot[style={ggreen,fill=ggreen,mark=none}]
    coordinates {(-O2,95.1) (-O3,92.5) (-O4,80.3)};

    \legend{GCC, LLVM-GCC}
\end{axis}
\end{tikzpicture}
\caption{Figure adopted from Lattner \cite{Lattner:2008:slides}}
\end{figure}
\end{frame}

\section{Performing Polyhedral Optimizations in LLVM}

\begin{frame}
    \frametitle{Literature for this part}
    \begin{itemize}
        \item \scriptsize\bibentry{Grosser:2012}
        \item \scriptsize\bibentry{Grosser:2011}
        \item \scriptsize\bibentry{Grosser:2011:slides}
        \item \scriptsize\bibentry{Lengauer:1993}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Motivation}
    \begin{itemize}
        \item Many possibilities for efficient execution on modern hardware:
            \begin{itemize}
                \item Vector instructions
                \item Vector accelerators and GPUs
                \item Multiple cores even on mobile platforms
            \end{itemize}
        \item Little compiler support to benefit from these
        \item Often manual optimization necessary
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Polly}
    \begin{itemize}
        \item A tool for automatic polyhedral optimizations
        \item Operates on LLVM intermediate representation
        \item Exploits paralellism via OpenMP and Vectorization
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{The Polyhedral Model}
    \begin{itemize}
        \item Computational model for programs
        \item Programs are represented by polyhedra
        \item Program transformations are modelled via mathematical operations
        \item Used for loop optimizations and parallelization
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Static Control Parts (SCoPs)}
    \begin{itemize}
        \item Only control flow structures are \lstinline[language=C++]!for!-loops and
            \lstinline[language=C++]!if!-statements
        \item Each loop has a single induction variable
        \item Induction variables are incremented from lower to upper bound by constant stride
        \item Only valid statements are assignments to array elements
        \item No/known sides effects
        \item \ldots
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Example: SCoP}
    \begin{figure}
\begin{lstlisting}[language=C++]
for (int i = 0; i <= N; i++) {
    A[i] = i;
    if(i <= 10)
        B[0] += i;
}
\end{lstlisting}
        \caption{Example adopted from Grosser \cite{Grosser:2012}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Architecture}
    \begin{figure}
        \centering
        \includegraphics[width=300pt]{resources/polly-architecture.png}
        \caption{Figure adopted from Grosser \cite{Grosser:2012}}
    \end{figure}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Polly's Polyhedral Representation}
    \begin{itemize}
        \item SCoP = (Context, [Statement])
        \item Context = Set of constraints on parameters\\
            (parameters are defined outside SCoPs and not changed inside)
        \item Statement = (Domain, Schedule, [Access])
        \pause
        \item Domain $\mathcal{D}$ = Set of loop iterations
            in which the statement is executed
        \pause
        \item Schedule $\mathcal{S}$ = Execution order of statements in
            final code
            \pause
        \item Access = (``Read'' $|$ ``Write'' $|$ \dots, $\mathcal{A}$)
        \item $\mathcal{A}$ defines the accessed memory space
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Example: Polly's Polyhedral Representation}
    \begin{figure}
\begin{lstlisting}[language=C++]
for (int i = 0; i <= N; i++) {
    A[i] = i;      // S1
    if(i <= 10)
        B[0] += i; // S2
}
\end{lstlisting}

        \begin{equation*}
            \mathit{Context} = \lbrace \left[ N \right] \rbrace
        \end{equation*}
        \vspace{-35pt}
        \begin{columns}[t,onlytextwidth]
            \begin{column}{0.48\textwidth}
                \begin{align*}
                    \mathcal{D}_{\mathit{S1}} &= \lbrace \mathit{S1} \left[
                    i \right] : 0 \leq i \leq N \rbrace\\
                    \mathcal{S}_{\mathit{S1}} &= \lbrace \mathit{S1} \left[
                    i \right] \rightarrow \left[ i, 0 \right] \rbrace\\
                    \mathit{Access}_{\mathit{S1}} &= \lbrace
                    \text{``Write''}, \mathcal{A}_{\mathit{S1}} \rbrace\\
                    \mathcal{A}_{\mathit{S1}} &= \lbrace \mathit{S1} \left[
                    i \right] \rightarrow A \left[ i \right] \rbrace
               \end{align*}
            \end{column}
            \begin{column}{0.48\textwidth}
                \begin{align*}
                    \mathcal{D}_{\mathit{S2}} &= \lbrace \mathit{S2} \left[
                    i \right] : 0 \leq i \leq 10 \rbrace\\
                    \mathcal{S}_{\mathit{S2}} &= \lbrace \mathit{S2} \left[
                    i \right] \rightarrow \left[ i, 1 \right] \rbrace\\
                    \mathit{Access}_{\mathit{S2}} &= \lbrace
                    \text{``Write''}, \mathcal{A}_{\mathit{S2}} \rbrace\\
                    \mathcal{A}_{\mathit{S2}} &= \lbrace \mathit{S2} \left[
                    i \right] \rightarrow B \left[ 0 \right] \rbrace
                \end{align*}
            \end{column}
        \end{columns}
        \caption{Example adopted from Grosser \cite{Grosser:2012}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{From Polyhedral SCoPs to LLVM IR}
    \begin{itemize}
        \item Sequential code is generated by default\\
            Takes advantage of data-locality, etc.
        \item Parallel code possible:
    \begin{itemize}
        \item OpenMP\\
            Takes advantage of thread-level parallelism
        \item Vectorization\\
            For small loops with small, constant number of iterations
    \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{How to Use Polly?}
    \begin{itemize}
        \item Manually use individual parts of Polly
        \item As part of LLVM
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Use Polly with Clang}
    \begin{itemize}
        \item Install Polly\\
            \url{http://polly.llvm.org/get_started.html}
        \item Invoke Clang with Polly
\begin{lstlisting}
$ clang -O3 some_program.c \
    -Xclang -load -Xclang LLVMPolly.so \
    -mllvm -polly
\end{lstlisting}
        \pause
        \item Generate OpenMP code
\begin{lstlisting}
$ clang ... -mllvm -polly-parallel -lgomp
\end{lstlisting}
        \pause
        \item Generate vectorized code
\begin{lstlisting}
$ clang ... -mllvm -polly-vectorizer=stripmine
\end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Speedup for Sequential Code}
    \begin{figure}
        \includegraphics[width=\textwidth]{resources/polly-sequential.png}
        \caption{Figure adopted from Grosser \cite{Grosser:2012}}
    \end{figure}
\end{frame}

\begin{frame}
\frametitle{Speedup for Parallel Code}
    \begin{figure}
        \includegraphics[width=\textwidth]{resources/polly-parallel1.png}
        \caption{Figure adopted from Grosser \cite{Grosser:2012}}
    \end{figure}
\end{frame}

\begin{frame}[t,allowframebreaks]
    \frametitle{References}
    \nocite{LLVM:2015}
    {\footnotesize\bibliography{references.bib}}
\end{frame}
